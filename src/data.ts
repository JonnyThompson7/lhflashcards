interface Data {
    front: {
        frontText: string
        frontImage: string
    }
    back: string
}

export const data: Data[] = [
    {
        front: {
            frontText: 'What does this attack do?',
            frontImage: '/images/VoidDarkness.png'
        },
        back: 'Darkness for 2 Seconds, 175 Armor Piercing damage'
    },
    {
        front: {
            frontText: 'What does this attack do?',
            frontImage: '/images/VoidParalyze.png'
        },
        back: 'Paralyzed for 1 Second, 185 damage'
    }
]
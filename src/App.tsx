import React from 'react'
import Flashcard from './Card'
import { data } from './data'

const App: React.FC = () => {
  const card = data[0]

  return (
    <>
      <h1>Hi</h1>
      <div>
        <Flashcard cardInfo={card} />
      </div>
    </>
  )
}
export default App

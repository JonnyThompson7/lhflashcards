import React from 'react'

const Flashcard: React.FC<any> = props => {
  console.log(props.cardInfo)
  return (
    <div>
      <div>{props.cardInfo.front.frontText}</div>
      <img alt='' src={props.cardInfo.front.frontImage} />
    </div>
  )
}

export default Flashcard
